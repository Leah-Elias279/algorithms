cmake_minimum_required(VERSION 3.10)
project(algorithms)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lmingw32")
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")

include_directories(.)
include_directories("Sorting Vizualization")

add_executable(algorithms
        BruteForceMatrix.cpp
        BruteForceMatrix.h
        DivideMatrix.cpp
        DivideMatrix.h
        main.cpp
        StrassenMatrix.cpp
        StrassenMatrix.h SDL_Plotter.h squareMatrix.h)

target_link_libraries(algorithms SDL2main SDL2 SDL2_mixer)

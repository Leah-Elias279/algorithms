#ifndef DIVIDEMATRIX_H_INCLUDED
#define DIVIDEMATRIX_H_INCLUDED

class DivideMatrix {
public:
    int ** multiply(int **, int **, int, int, int, int, int);
    int ** add(int **, int **, int);
};

#endif // DIVIDEMATRIX_H_INCLUDED

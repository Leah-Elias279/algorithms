#include <iostream>
#include <time.h>
#include <iomanip>
#include "squareMatrix.h"

using namespace std;

int main() {
    SquareMatrix A;
    SquareMatrix* C;

    A.dim = 1024;
    //B.dim = 4;

    A.data = new int* [1024];
    //B.data = new int* [4];

    for(int i = 0; i < 1024; i++) {
        A.data[i] = new int[1024];
        //B.data[i] = new int[4];
    }

    for(int i = 0; i < 1024; i++) {
        for (int j = 0; j < 1024; j++) {
            A.data[i][j] = i+j;
           //B.data[i][j] = i+j;
        }
    }

    /*for(int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << A.data[i][j] << endl;
            //cout << B.data[i][j] << endl;
        }
    }*/

    cout << setprecision(2) << fixed;

    time_t start, end;
    start = clock();
    C = ThreadedDivideAndConquer(A, A);
    end = clock();

    double time = difftime(end, start) / 5;

    cout << time / CLOCKS_PER_SEC << endl;

    start = clock();
    C = DivideAndConquer(A, A);
    end = clock();

    time = difftime(end, start);

    cout << time / CLOCKS_PER_SEC << endl;

    /*for(int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            cout << C->data[i][j];
        }
        cout << endl;
    }*/

    return 0;
}
//
// Created by colecam on 11/17/2018.
//

#include "StrassenMatrix.h"
#include <iostream>

using namespace std;

int** Strassen::strassen(int ** n1, int ** n2, int size){
    if(size == 2){
        int** array;

        array = new int*[size];

        for(int i = 0; i < size; i++){
            array[i] = new int[size];
        }

        int p1 = n1[0][0] * (n2[0][1] - n2[1][1]);
        int p2 = (n1[0][0] + n1[0][1]) * n2[1][1];
        int p3 = (n1[1][0] + n1[1][1]) * n2[0][0];
        int p4 = n1[1][1] * (n2[1][0] - n2[0][0]);
        int p5 = (n1[0][0] + n1[1][1]) * (n2[0][0] + n2[1][1]);
        int p6 = (n1[0][1] - n1[1][1]) * (n2[0][1] + n2[1][1]);
        int p7 = (n1[0][0] - n1[1][0]) * (n2[0][0] + n2[0][1]);

        array[0][0] = p5 + p4 - p2 + p6;
        array[0][1] = p1 + p2;
        array[1][0] = p3 + p4;
        array[1][1] = p5 + p1 - p3 - p7;

        return array;
    }

    int** a00;
    int** a01;
    int** a10;
    int** a11;

    int** b00;
    int** b01;
    int** b10;
    int** b11;

    a00 = new int*[size/2];
    a01 = new int*[size/2];
    a10 = new int*[size/2];
    a11 = new int*[size/2];

    b00 = new int*[size/2];
    b01 = new int*[size/2];
    b10 = new int*[size/2];
    b11 = new int*[size/2];

    for(int i = 0; i < size; i++){
        a00[i] = new int[size/2];
        a01[i] = new int[size/2];
        a10[i] = new int[size/2];
        a11[i] = new int[size/2];

        b00[i] = new int[size/2];
        b01[i] = new int[size/2];
        b10[i] = new int[size/2];
        b11[i] = new int[size/2];
    }

    for(int i = 0; i < size / 2; i++){
        for(int j = 0; j < size / 2; j++){
            a00[i][j] = n1[i][j];
            a10[i][j] = n1[i + (size/2)][j];
            a01[i][j] = n1[i][j+(size/2)];
            a11[i][j] = n1[i+(size/2)][j+(size/2)];

            b00[i][j] = n2[i][j];
            b10[i][j] = n2[i + (size/2)][j];
            b01[i][j] = n2[i][j+(size/2)];
            b11[i][j] = n2[i+(size/2)][j+(size/2)];
        }
    }

    strassen(a00, b00, size/2);
    strassen(a01, b01, size/2);
    strassen(a10, b10, size/2);
    strassen(a11, b11, size/2);
}


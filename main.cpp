#include <iostream>
#include <ctime>
#include <cmath>
#include "squareMatrix.h"

using namespace std;

int main(int argc, char **argv)
{
    int m = 2;
    double avg = 0;


    while(m<500) {
        for(int n=0;n<100;n++) {
            SquareMatrix temp;

            temp.dim = m;

            temp.data = new int* [m];

            for(int i=0;i<m;i++) {
                temp.data[i] = new int[m];
                //cout << "testing" << endl;
                for(int j=0;j<m;j++) {
                    temp.data[i][j] = i+j;
                }
            }


            time_t start = clock();
            BruteForce(temp, temp);
            time_t finish = clock();

            avg += difftime(finish, start);
        }

        cout << (avg / 100) << endl;

        avg = 0;

        m *= 2;
    }


    m = 2;
    while(m<500) {
        for(int n=0;n<100;n++) {
            SquareMatrix temp;

            temp.dim = m;

            temp.data = new int* [m];

            for(int i=0;i<m;i++) {
                temp.data[i] = new int[m];
                //cout << "testing" << endl;
                for(int j=0;j<m;j++) {
                    temp.data[i][j] = i+j;
                }
            }


            time_t start = clock();
            DivideAndConquer(temp, temp);
            time_t finish = clock();

            avg += difftime(finish, start);
        }

        cout << (avg / 100) << endl;

        avg = 0;

        m *= 2;
    }


    m = 2;
    while(m < 500){
        for(int n = 0; n < 100; n++){
            SquareMatrix temp;

            temp.dim = m;
            temp.data = new int* [m];

            for(int i = 0; i < m; i++){
                temp.data[i] = new int[m];
                for(int j = 0; j < m; j++){
                    temp.data[i][j] = i+j;
                }
            }
            time_t start = clock();
            Strassen(temp, temp);
            time_t finish = clock();

            avg += difftime(finish, start);
        }
        cout << (avg / 100) << endl;

        avg = 0;

        m *= 2;
    }


    m = 2;
    while(m<500) {
        for(int n=0;n<100;n++) {
            SquareMatrix temp;

            temp.dim = m;

            temp.data = new int* [m];

            for(int i=0;i<m;i++) {
                temp.data[i] = new int[m];
                //cout << "testing" << endl;
                for(int j=0;j<m;j++) {
                    temp.data[i][j] = i+j;
                }
            }


            time_t start = clock();
            ThreadedDivideAndConquer(temp, temp);
            time_t finish = clock();

            avg += difftime(finish, start);
        }

        cout << (avg / 100) << endl;

        avg = 0;

        m *= 2;
    }

    /*time_t finish = clock();
    cout << "Brute force: " << finish - start << endl;
    cout << endl << endl << endl << endl;

    start = clock();
    for(int n=0;n<10;n++) {
        m=2;
        while(m<500) {
            cout << "M: " << m << endl;
            int ** temp = new int*[m];
            DivideMatrix dm;
            for(int i=0;i<m;i++) {
                temp[i] = new int[m];
                for(int j=0;j<m;j++) {
                    temp[i][j] = i*j+1;
                }
            }
            dm.multiply(temp,temp,0,0,0,0,m);
            /*for(int i=0;i<m;i++) {
                for(int j=0;j<m;j++) {
                    cout << temp[i][j] << " ";
                }
                cout << endl;
            }*/ /*
            for(int i=0;i<m;i++) {
                delete temp[i];
            }
            m *= 2;
        }
    }
    finish = clock();
    cout << "Divide and conquer: " << finish - start << endl;*/
    return 0;
}

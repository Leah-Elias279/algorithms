#include "BruteForceMatrix.h"

int ** BruteForceMatrix::multiply(int ** n1, int ** n2, int size) {
    int ** temp = new int*[size];
    int num = 0;
    for(int i=0;i<size;i++) {
        temp[i] = new int[size];
    }
    for(int i=0;i<size;i++) {
        for(int j=0;j<size;j++) {
            for(int k=0;k<size;k++) {
                num += n1[i][k] * n2[k][j];
            }
            temp[i][j] = num;
            num = 0;
        }
    }
    return temp;
}

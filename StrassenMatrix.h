//
// Created by colecam on 11/17/2018.
//

#ifndef ALGORITHMS_STRASSENMATRIX_H
#define ALGORITHMS_STRASSENMATRIX_H

class Strassen {
public:
    int** strassen(int ** n1, int ** n2, int size);
};

#endif //ALGORITHMS_STRASSENMATRIX_H

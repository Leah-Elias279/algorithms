#include "DivideMatrix.h"

int ** DivideMatrix::multiply(int ** n1, int ** n2, int r1, int c1, int r2, int c2, int size) {
    int ** temp = new int*[size];
    for(int i=0;i<size;i++) {
        temp[i] = new int[size];
    }
    if(size == 1) {
        temp[0][0] = n1[r1][c1] * n2[r2][c2];
    }
    else {
        int ** x1 = multiply(n1,n2,r1,c1,r2,c2,size/2);
        int ** x2 = multiply(n1,n2,r1,c1+size/2,r2+size/2,c2,size/2);
        int ** x3 = multiply(n1,n2,r1,c1,r2,c2+size/2,size/2);
        int ** x4 = multiply(n1,n2,r1,c1+size/2,r2+size/2,c2+size/2,size/2);
        int ** x5 = multiply(n1,n2,r1+size/2,c1,r2,c2,size/2);
        int ** x6 = multiply(n1,n2,r1+size/2,c2+size/2,r2+size/2,c2,size/2);
        int ** x7 = multiply(n1,n2,r1+size/2,c1,r2,c2+size/2,size/2);
        int ** x8 = multiply(n1,n2,r1+size/2,c1+size/2,r2+size/2,c2+size/2,size/2);
        int ** q1 = add(x1,x2,size/2);
        int ** q2 = add(x3,x4,size/2);
        int ** q3 = add(x5,x6,size/2);
        int ** q4 = add(x7,x8,size/2);
        for(int i=0;i<size/2;i++) {
            for(int j=0;j<size/2;j++) {
                temp[i][j] = q1[i][j];
                temp[i+size/2][j] = q2[i][j];
                temp[i][j+size/2] = q3[i][j];
                temp[i+size/2][j+size/2] = q4[i][j];
            }
            delete x1[i];
            delete x2[i];
            delete x3[i];
            delete x4[i];
            delete x5[i];
            delete x6[i];
            delete x7[i];
            delete x8[i];
            delete q1[i];
            delete q2[i];
            delete q3[i];
            delete q4[i];
        }
    }
    return temp;
}

int ** DivideMatrix::add(int ** n1, int ** n2, int size) {
    int ** temp = new int*[size];
    for(int i=0;i<size;i++) {
        temp[i] = new int[size];
        for(int j=0;j<size;j++) {
            temp[i][j] = n1[i][j] + n2[i][j];
        }
    }
    return temp;
}

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/Algorithms Proj/algorithms/BruteForceMatrix.cpp" "E:/Algorithms Proj/algorithms/cmake-build-debug/CMakeFiles/algorithms.dir/BruteForceMatrix.cpp.obj"
  "E:/Algorithms Proj/algorithms/DivideMatrix.cpp" "E:/Algorithms Proj/algorithms/cmake-build-debug/CMakeFiles/algorithms.dir/DivideMatrix.cpp.obj"
  "E:/Algorithms Proj/algorithms/StrassenMatrix.cpp" "E:/Algorithms Proj/algorithms/cmake-build-debug/CMakeFiles/algorithms.dir/StrassenMatrix.cpp.obj"
  "E:/Algorithms Proj/algorithms/main.cpp" "E:/Algorithms Proj/algorithms/cmake-build-debug/CMakeFiles/algorithms.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../."
  "../Sorting Vizualization"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

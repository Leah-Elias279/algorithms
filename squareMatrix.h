/*
 * Author:                 Weston Straw
 * Assignment Title:       Project 3 - Matrix Multiplication
 * Assignment Description: Threaded and unthreaded implementations of matrix
 *                         multiplication to compare run times
 * Due Date:               02/26/2018
 * Date Created:           02/21/2018
 * Date Last Modified:     02/25/2018
 */

#ifndef MATRIX_MULTIPLICATION_SQUAREMATRIX_H
#define MATRIX_MULTIPLICATION_SQUAREMATRIX_H

#include <iostream>
#include <pthread.h>

using namespace std;

struct SquareMatrix{
    int dim;

    int** data;  // points to a [dim x dim] square matrix
};

struct TripleMatrix{
    SquareMatrix *A = new SquareMatrix;
    SquareMatrix *B = new SquareMatrix;
    SquareMatrix *C = new SquareMatrix;
};

/*
 * description: Add two square matrices of the same size together
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and have the same dimension
 * postcondition: returns the result of the matrix addition as a pointer
 */
SquareMatrix* Addition(const SquareMatrix A, const SquareMatrix B) {
    int dim = A.dim;

    SquareMatrix* C = new SquareMatrix;

    C->dim = dim;

    C->data = new int* [dim];

    for(int i = 0; i < dim; i++) {
        C->data[i] = new int [dim];
        for(int j = 0; j < dim; j++) {
            C->data[i][j] = A.data[i][j] + B.data[i][j];
        }
    }

    return C;
}

/*
 * description: Add two square matrices of the same size together
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and have the same dimension
 * postcondition: returns the result of the matrix addition as a pointer
 */
SquareMatrix* PointAdd(const SquareMatrix *A, const SquareMatrix *B) {
    int dim = A->dim;

    SquareMatrix* C = new SquareMatrix;

    C->dim = dim;

    C->data = new int* [dim];

    for(int i = 0; i < dim; i++) {
        C->data[i] = new int [dim];
        for(int j = 0; j < dim; j++) {
            C->data[i][j] = A->data[i][j] + B->data[i][j];
        }
    }

    return C;
}

/*
 * description: Add two square matrices of the same size together
 * return: SquareMatrix
 * precondition: SquareMatrix A and B exist and have the same dimension
 * postcondition: returns the result of the matrix addition as a SquareMatrix
 */
SquareMatrix StanAdd(const SquareMatrix A, const SquareMatrix B) {
    int dim = A.dim;

    SquareMatrix C;

    C.dim = dim;

    C.data = new int* [dim];

    for(int i = 0; i < dim; i++) {
        C.data[i] = new int [dim];
        for(int j = 0; j < dim; j++) {
            C.data[i][j] = A.data[i][j] + B.data[i][j];
        }
    }

    return C;
}

/*
 * description: Subtract two square matrices of the same size, B from A
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and have the same dimension
 * postcondition: returns result of the matrix subtraction as a pointer
 */
SquareMatrix* PointSub(const SquareMatrix* A, const SquareMatrix* B) {
    int dim = A->dim;

    SquareMatrix* C = new SquareMatrix;

    C->dim = dim;

    C->data = new int* [dim];

    for(int i = 0; i < dim; i++) {
        C->data[i] = new int [dim];
        for(int j = 0; j < dim; j++) {
            C->data[i][j] = A->data[i][j] - B->data[i][j];
        }
    }

    return C;
}

/*
 * description: Subtract two square matrices of the same size, B from A
 * return: SquareMatrix
 * precondition: SquareMatrix A and B exist and have the same dimension
 * postcondition: returns result of the matrix subtraction as a SquareMatrix
 */
SquareMatrix StanSub(const SquareMatrix A, const SquareMatrix B) {
    int dim = A.dim;

    SquareMatrix C;

    C.dim = dim;

    C.data = new int* [dim];

    for(int i = 0; i < dim; i++) {
        C.data[i] = new int [dim];
        for(int j = 0; j < dim; j++) {
            C.data[i][j] = A.data[i][j] - B.data[i][j];
        }
    }

    return C;
}

/*
 * description: Multiplies two square matrices together using teh brute force
 *              method of calculating each individual position in the new
 *              matrix to be returned. Set up for threading purposes.
 * return: void *
 * precondition: param has an existing SquareMatrix A, B and C, where A and B
 *               will be multiplied and the result stored in C. All must be the
 *               same size as well
 * postcondition: stores the resulting matrix in C
 */
void * BruteForceSquareMatrixMultiplication(void *param) {
    TripleMatrix* brute = (TripleMatrix*) param;

    brute->C->dim = brute->A->dim;

    brute->C->data = new int* [brute->A->dim];
    for(int i = 0; i < brute->C->dim; i++) {
        brute->C->data[i] = new int[brute->A->dim];
        for(int j = 0; j < brute->C->dim; j++) {
            brute->C->data[i][j] = 0;
            for(int k = 0; k < brute->C->dim; k++) {
                brute->C->data[i][j] += brute->A->data[i][k] * brute->B->data[k][j];
            }
        }
    }
}

/*
 * description: Multiplies two square matrices together using teh brute force
 *              method of calculating each individual position in the new
 *              matrix to be returned.
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and are the same size
 * postcondition: returns the resulting matrix as a pointer
 */
SquareMatrix* BruteForce(const SquareMatrix& A, const SquareMatrix& B) {
    SquareMatrix* C = new SquareMatrix;
    C->dim = A.dim;

    C->data = new int* [A.dim];

    for(int i = 0; i < C->dim; i++) {
        C->data[i] = new int[A.dim];
        for(int j = 0; j < C->dim; j++) {
            C->data[i][j] = 0;
            for(int k = 0; k < C->dim; k++) {
                C->data[i][j] += A.data[i][k] * B.data[k][j];
            }
        }
    }

    // Returns final value of C, the calculated matrix multiplication
    return C;
}

SquareMatrix* DivideAndConquer(const SquareMatrix& A,
                                       const SquareMatrix& B)
{
    // Creates the matrix to store the result
    SquareMatrix *C = new SquareMatrix;

    // Creates eight threads to call the threaded version of brute force eight
    // times in order to speed up calculations
    //pthread_t thread1, thread2, thread3, thread4, thread5, thread6, thread7;
    //pthread_t thread8;

    // Sets dimension of C
    C->dim = A.dim;

    // If it is a one by one matrix, then find the result using a single,
    // regular multiplication
    if(A.dim == 1) {
        C->data = new int* [1];
        C->data[0] = new int[1];
        C->data[0][0] = A.data[0][0] * B.data[0][0];
    } else {
        // Otherwise, prepare to find result using threading
        int sub = A.dim / 2;

        // Square matrices created to subdivide matrices A, B, and C into four
        // parts to make calculations easier and less intensive.  Pieces the
        // parts of C together to find final result.
        SquareMatrix *A11 = new SquareMatrix;
        SquareMatrix *A12 = new SquareMatrix;
        SquareMatrix *A21 = new SquareMatrix;
        SquareMatrix *A22 = new SquareMatrix;

        SquareMatrix *B11 = new SquareMatrix;
        SquareMatrix *B12 = new SquareMatrix;
        SquareMatrix *B21 = new SquareMatrix;
        SquareMatrix *B22 = new SquareMatrix;

        SquareMatrix *C11 = new SquareMatrix;
        SquareMatrix *C12 = new SquareMatrix;
        SquareMatrix *C21 = new SquareMatrix;
        SquareMatrix *C22 = new SquareMatrix;

        // The temporary matrices are created to hold the pieces of each C
        // subdivision before adding them together to create the whole
        // subdivision for later use
        SquareMatrix *tempC11A = new SquareMatrix;
        SquareMatrix *tempC11B = new SquareMatrix;
        SquareMatrix *tempC12A = new SquareMatrix;
        SquareMatrix *tempC12B = new SquareMatrix;
        SquareMatrix *tempC21A = new SquareMatrix;
        SquareMatrix *tempC21B = new SquareMatrix;
        SquareMatrix *tempC22A = new SquareMatrix;
        SquareMatrix *tempC22B = new SquareMatrix;

        // Sets all necessary dimensions of the matrices
        A11->dim = A12->dim = A21->dim = A22->dim = B11->dim = B12->dim = sub;
        B21->dim = B22->dim = C11->dim = C12->dim = C21->dim = C22->dim = sub;

        // Initializes the double array data in each matrix to the correct size
        // to ensure no out of bounds error
        A11->data = new int* [sub];
        A12->data = new int* [sub];
        A21->data = new int* [sub];
        A22->data = new int* [sub];

        B11->data = new int* [sub];
        B12->data = new int* [sub];
        B21->data = new int* [sub];
        B22->data = new int* [sub];

        for (int i = 0; i < sub; i++) {
            A11->data[i] = new int[sub];
            A12->data[i] = new int [sub];
            A21->data[i] = new int [sub];
            A22->data[i] = new int [sub];

            B11->data[i] = new int [sub];
            B12->data[i] = new int [sub];
            B21->data[i] = new int [sub];
            B22->data[i] = new int [sub];

            // After finishing initializing the size of data, then populates
            // the four subdivisions of A and B with the corresponding values
            for (int j = 0; j < sub; j++) {
                A11->data[i][j] = A.data[i][j];
                A12->data[i][j] = A.data[i][j + sub];
                A21->data[i][j] = A.data[i + sub][j];
                A22->data[i][j] = A.data[i + sub][j + sub];

                B11->data[i][j] = B.data[i][j];
                B12->data[i][j] = B.data[i][j + sub];
                B21->data[i][j] = B.data[i + sub][j];
                B22->data[i][j] = B.data[i + sub][j + sub];
            }
        }

        // Creates eight triple matrices to pass to the threading function of
        // brute force. Each triple matrix holds the necessary three matrices
        // to calculate and store a piece of C's four subdivisions
        TripleMatrix tmC111;
        TripleMatrix tmC112;
        TripleMatrix tmC121;
        TripleMatrix tmC122;
        TripleMatrix tmC211;
        TripleMatrix tmC212;
        TripleMatrix tmC221;
        TripleMatrix tmC222;

        // Prepares each triple matrix with the necessary matrices for
        // threading
        /*tmC111.A = A11;
        tmC111.B = B11;
        tmC111.C = tempC11A;

        tmC112.A = A12;
        tmC112.B = B21;
        tmC112.C = tempC11B;

        tmC121.A = A11;
        tmC121.B = B12;
        tmC121.C = tempC12A;

        tmC122.A = A12;
        tmC122.B = B22;
        tmC122.C = tempC12B;

        tmC211.A = A21;
        tmC211.B = B11;
        tmC211.C = tempC21A;

        tmC212.A = A22;
        tmC212.B = B21;
        tmC212.C = tempC21B;

        tmC221.A = A21;
        tmC221.B = B12;
        tmC221.C = tempC22A;

        tmC222.A = A22;
        tmC222.B = B22;
        tmC222.C = tempC22B;*/

        tempC11A = BruteForce(*A11, *B11);
        tempC11B = BruteForce(*A12, *B21);
        tempC12A = BruteForce(*A11, *B22);
        tempC12B = BruteForce(*A12, *B22);
        tempC21A = BruteForce(*A21, *B11);
        tempC21B = BruteForce(*A22, *B21);
        tempC22A = BruteForce(*A21, *B12);
        tempC22B = BruteForce(*A22, *B22);


        // Threads each call to the threaded brute force function to decrease
        // time necessary to calculate the multiplication by performing all
        // eight of the multiplications at once
        /*pthread_create(&thread1, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC111);
        pthread_create(&thread2, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC112);
        pthread_create(&thread3, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC121);
        pthread_create(&thread4, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC122);
        pthread_create(&thread5, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC211);
        pthread_create(&thread6, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC212);
        pthread_create(&thread7, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC221);
        pthread_create(&thread8, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC222);

        // Waits until all threads are finished and results have been stored
        pthread_join(thread1, nullptr);
        pthread_join(thread2, nullptr);
        pthread_join(thread3, nullptr);
        pthread_join(thread4, nullptr);
        pthread_join(thread5, nullptr);
        pthread_join(thread6, nullptr);
        pthread_join(thread7, nullptr);
        pthread_join(thread8, nullptr);*/

        // Calculates each part of C's four subdivisions
        C11 = Addition(*tempC11A, *tempC11B);
        C12 = Addition(*tempC12A, *tempC12B);
        C21 = Addition(*tempC21A, *tempC21B);
        C22 = Addition(*tempC22A, *tempC22B);

        // Initializes the size of C's double array of data
        C->data = new int* [A.dim];
        for(int i = 0; i < A.dim; i++) {
            C->data[i] = new int[A.dim];
        }

        // Populates C using the four calculated subdivisions
        for (int i = 0; i < sub; i++) {
            for (int j = 0; j < sub; j++) {
                C->data[i][j] = C11->data[i][j];
                C->data[i][j + sub] = C12->data[i][j];
                C->data[i + sub][j] = C21->data[i][j];
                C->data[i + sub][j + sub] = C22->data[i][j];
            }
        }
    }

    // Returns final value of C, the calculated matrix multiplication
    return C;
}

/*
 * description: Multiplies two square matrices together using a divide and
 *              conquer method that requires only eight multiplications to find
 *              four subsections of the resulting matrix and then pieces them
 *              together
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and are the same size
 * postcondition: returns the resulting matrix as a pointer
 */
SquareMatrix* ThreadedDivideAndConquer(const SquareMatrix& A,
                                       const SquareMatrix& B)
{
    // Creates the matrix to store the result
    SquareMatrix *C = new SquareMatrix;

    // Creates eight threads to call the threaded version of brute force eight
    // times in order to speed up calculations
    pthread_t thread1, thread2, thread3, thread4, thread5, thread6, thread7;
    pthread_t thread8;

    // Sets dimension of C
    C->dim = A.dim;

    // If it is a one by one matrix, then find the result using a single,
    // regular multiplication
    if(A.dim == 1) {
        C->data = new int* [1];
        C->data[0] = new int[1];
        C->data[0][0] = A.data[0][0] * B.data[0][0];
    } else {
        // Otherwise, prepare to find result using threading
        int sub = A.dim / 2;

        // Square matrices created to subdivide matrices A, B, and C into four
        // parts to make calculations easier and less intensive.  Pieces the
        // parts of C together to find final result.
        SquareMatrix *A11 = new SquareMatrix;
        SquareMatrix *A12 = new SquareMatrix;
        SquareMatrix *A21 = new SquareMatrix;
        SquareMatrix *A22 = new SquareMatrix;

        SquareMatrix *B11 = new SquareMatrix;
        SquareMatrix *B12 = new SquareMatrix;
        SquareMatrix *B21 = new SquareMatrix;
        SquareMatrix *B22 = new SquareMatrix;

        SquareMatrix *C11 = new SquareMatrix;
        SquareMatrix *C12 = new SquareMatrix;
        SquareMatrix *C21 = new SquareMatrix;
        SquareMatrix *C22 = new SquareMatrix;

        // The temporary matrices are created to hold the pieces of each C
        // subdivision before adding them together to create the whole
        // subdivision for later use
        SquareMatrix *tempC11A = new SquareMatrix;
        SquareMatrix *tempC11B = new SquareMatrix;
        SquareMatrix *tempC12A = new SquareMatrix;
        SquareMatrix *tempC12B = new SquareMatrix;
        SquareMatrix *tempC21A = new SquareMatrix;
        SquareMatrix *tempC21B = new SquareMatrix;
        SquareMatrix *tempC22A = new SquareMatrix;
        SquareMatrix *tempC22B = new SquareMatrix;

        // Sets all necessary dimensions of the matrices
        A11->dim = A12->dim = A21->dim = A22->dim = B11->dim = B12->dim = sub;
        B21->dim = B22->dim = C11->dim = C12->dim = C21->dim = C22->dim = sub;

        // Initializes the double array data in each matrix to the correct size
        // to ensure no out of bounds error
        A11->data = new int* [sub];
        A12->data = new int* [sub];
        A21->data = new int* [sub];
        A22->data = new int* [sub];

        B11->data = new int* [sub];
        B12->data = new int* [sub];
        B21->data = new int* [sub];
        B22->data = new int* [sub];

        for (int i = 0; i < sub; i++) {
            A11->data[i] = new int[sub];
            A12->data[i] = new int [sub];
            A21->data[i] = new int [sub];
            A22->data[i] = new int [sub];

            B11->data[i] = new int [sub];
            B12->data[i] = new int [sub];
            B21->data[i] = new int [sub];
            B22->data[i] = new int [sub];

            // After finishing initializing the size of data, then populates
            // the four subdivisions of A and B with the corresponding values
            for (int j = 0; j < sub; j++) {
                A11->data[i][j] = A.data[i][j];
                A12->data[i][j] = A.data[i][j + sub];
                A21->data[i][j] = A.data[i + sub][j];
                A22->data[i][j] = A.data[i + sub][j + sub];

                B11->data[i][j] = B.data[i][j];
                B12->data[i][j] = B.data[i][j + sub];
                B21->data[i][j] = B.data[i + sub][j];
                B22->data[i][j] = B.data[i + sub][j + sub];
            }
        }

        // Creates eight triple matrices to pass to the threading function of
        // brute force. Each triple matrix holds the necessary three matrices
        // to calculate and store a piece of C's four subdivisions
        TripleMatrix tmC111;
        TripleMatrix tmC112;
        TripleMatrix tmC121;
        TripleMatrix tmC122;
        TripleMatrix tmC211;
        TripleMatrix tmC212;
        TripleMatrix tmC221;
        TripleMatrix tmC222;

        // Prepares each triple matrix with the necessary matrices for
        // threading
        tmC111.A = A11;
        tmC111.B = B11;
        tmC111.C = tempC11A;

        tmC112.A = A12;
        tmC112.B = B21;
        tmC112.C = tempC11B;

        tmC121.A = A11;
        tmC121.B = B12;
        tmC121.C = tempC12A;

        tmC122.A = A12;
        tmC122.B = B22;
        tmC122.C = tempC12B;

        tmC211.A = A21;
        tmC211.B = B11;
        tmC211.C = tempC21A;

        tmC212.A = A22;
        tmC212.B = B21;
        tmC212.C = tempC21B;

        tmC221.A = A21;
        tmC221.B = B12;
        tmC221.C = tempC22A;

        tmC222.A = A22;
        tmC222.B = B22;
        tmC222.C = tempC22B;

        /*tempC11A = BruteForce(*A11, *B11);
        tempC11B = BruteForce(*A12, *B21);
        tempC12A = BruteForce(*A11, *B22);
        tempC12B = BruteForce(*A12, *B22);
        tempC21A = BruteForce(*A21, *B11);
        tempC21B = BruteForce(*A22, *B21);
        tempC22A = BruteForce(*A21, *B12);
        tempC22B = BruteForce(*A22, *B22);*/


        // Threads each call to the threaded brute force function to decrease
        // time necessary to calculate the multiplication by performing all
        // eight of the multiplications at once
        pthread_create(&thread1, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC111);
        pthread_create(&thread2, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC112);
        pthread_create(&thread3, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC121);
        pthread_create(&thread4, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC122);
        pthread_create(&thread5, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC211);
        pthread_create(&thread6, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC212);
        pthread_create(&thread7, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC221);
        pthread_create(&thread8, nullptr, BruteForceSquareMatrixMultiplication,
                       &tmC222);

        // Waits until all threads are finished and results have been stored
        pthread_join(thread1, nullptr);
        pthread_join(thread2, nullptr);
        pthread_join(thread3, nullptr);
        pthread_join(thread4, nullptr);
        pthread_join(thread5, nullptr);
        pthread_join(thread6, nullptr);
        pthread_join(thread7, nullptr);
        pthread_join(thread8, nullptr);

        // Calculates each part of C's four subdivisions
        C11 = Addition(*tempC11A, *tempC11B);
        C12 = Addition(*tempC12A, *tempC12B);
        C21 = Addition(*tempC21A, *tempC21B);
        C22 = Addition(*tempC22A, *tempC22B);

        // Initializes the size of C's double array of data
        C->data = new int* [A.dim];
        for(int i = 0; i < A.dim; i++) {
            C->data[i] = new int[A.dim];
        }

        // Populates C using the four calculated subdivisions
        for (int i = 0; i < sub; i++) {
            for (int j = 0; j < sub; j++) {
                C->data[i][j] = C11->data[i][j];
                C->data[i][j + sub] = C12->data[i][j];
                C->data[i + sub][j] = C21->data[i][j];
                C->data[i + sub][j + sub] = C22->data[i][j];
            }
        }
    }

    // Returns final value of C, the calculated matrix multiplication
    return C;
}

/*
 * description: Multiplies two square matrices together using Strassen's method
 *              that requires only seven multiplications to find four
 *              subsections of the resulting matrix and then pieces them
 *              together
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and are the same size
 * postcondition: returns the resulting matrix as a pointer
 */
SquareMatrix* Strassen(const SquareMatrix& A, const SquareMatrix& B) {
    SquareMatrix *C = new SquareMatrix;

    // Sets dimension of C
    C->dim = A.dim;

    // If it is a one by one matrix, then find the result using a single,
    // regular multiplication
    if(A.dim == 1) {
        C->data = new int* [A.dim];
        C->data[0] = new int[A.dim];
        C->data[0][0] = A.data[0][0] * B.data[0][0];
    } else {
        // Otherwise, prepare to find result using Strassen's method
        int sub = A.dim / 2;

        // Square matrices created to subdivide matrices A, B, and C into four
        // parts to make calculations easier and less intensive.  Pieces the
        // parts of C together to find final result.
        SquareMatrix A11;
        SquareMatrix A12;
        SquareMatrix A21;
        SquareMatrix A22;

        SquareMatrix B11;
        SquareMatrix B12;
        SquareMatrix B21;
        SquareMatrix B22;

        SquareMatrix *C11 = new SquareMatrix;
        SquareMatrix *C12 = new SquareMatrix;
        SquareMatrix *C21 = new SquareMatrix;
        SquareMatrix *C22 = new SquareMatrix;

        // Square Matrices P1-P7 and S1-S10 are created to implement Strassen's
        // method. S1-S10 is used to calculated the P1-P7 matrices, while P1-P7
        // are used to only multiply seven times to find the subdivisions of C
        SquareMatrix *P1 = new SquareMatrix;
        SquareMatrix *P2 = new SquareMatrix;
        SquareMatrix *P3 = new SquareMatrix;
        SquareMatrix *P4 = new SquareMatrix;
        SquareMatrix *P5 = new SquareMatrix;
        SquareMatrix *P6 = new SquareMatrix;
        SquareMatrix *P7 = new SquareMatrix;

        SquareMatrix S1;
        SquareMatrix S2;
        SquareMatrix S3;
        SquareMatrix S4;
        SquareMatrix S5;
        SquareMatrix S6;
        SquareMatrix S7;
        SquareMatrix S8;
        SquareMatrix S9;
        SquareMatrix S10;

        // Sets all necessary dimensions of the matrices
        A11.dim = A12.dim = A21.dim = A22.dim = B11.dim = B12.dim = sub;
        B21.dim = B22.dim = C11->dim = C12->dim = C21->dim = C22->dim = sub;
        P1->dim = P2->dim = P3->dim = P4->dim = P5->dim = P6->dim = sub;
        P7->dim = S1.dim = S2.dim = S3.dim = S4.dim = S5.dim = S6.dim = sub;
        S7.dim = S8.dim = S9.dim = S10.dim = sub;

        // Initializes the double array data in each matrix to the correct size
        // to ensure no out of bounds error
        A11.data = new int *[sub];
        A12.data = new int *[sub];
        A21.data = new int *[sub];
        A22.data = new int *[sub];

        B11.data = new int *[sub];
        B12.data = new int *[sub];
        B21.data = new int *[sub];
        B22.data = new int *[sub];


        for (int i = 0; i < sub; i++) {
            A11.data[i] = new int[sub];
            A12.data[i] = new int[sub];
            A21.data[i] = new int[sub];
            A22.data[i] = new int[sub];

            B11.data[i] = new int[sub];
            B12.data[i] = new int[sub];
            B21.data[i] = new int[sub];
            B22.data[i] = new int[sub];

            // After finishing initializing the size of data, then populates
            // the four subdivisions of A and B with the corresponding values
            for (int j = 0; j < sub; j++) {
                A11.data[i][j] = A.data[i][j];
                A12.data[i][j] = A.data[i][j + sub];
                A21.data[i][j] = A.data[i + sub][j];
                A22.data[i][j] = A.data[i + sub][j + sub];

                B11.data[i][j] = B.data[i][j];
                B12.data[i][j] = B.data[i][j + sub];
                B21.data[i][j] = B.data[i + sub][j];
                B22.data[i][j] = B.data[i + sub][j + sub];
            }
        }

        // Calculates each individual S1-S10 using addition or subtraction
        // functions for matrices. Standard add and sub are used here because
        // S1-S10 are SquareMatrices and not a pointer
        S1 = StanSub(B12, B22);
        S2 = StanAdd(A11, A12);
        S3 = StanAdd(A21, A22);
        S4 = StanSub(B21, B11);
        S5 = StanAdd(A11, A22);
        S6 = StanAdd(B11, B22);
        S7 = StanSub(A12, A22);
        S8 = StanAdd(B21, B22);
        S9 = StanSub(A11, A21);
        S10 = StanAdd(B11, B12);

        // Calls the standard brut force multiplication seven times to
        // calculate the value of P1-P7
        P1 = BruteForce(A11, S1);
        P2 = BruteForce(S2, B22);
        P3 = BruteForce(S3, B11);
        P4 = BruteForce(A22, S4);
        P5 = BruteForce(S5, S6);
        P6 = BruteForce(S7, S8);
        P7 = BruteForce(S9, S10);

        // Uses P1-P7 to find the subdivisions of C
        // Point sub and add are used because pointers are given as the
        // parameters and a pointer is returned.
        C11 = PointAdd(P5, P4);
        C11 = PointSub(C11, P2);
        C11 = PointAdd(C11, P6);

        C12 = PointAdd(P1, P2);
        C21 = PointAdd(P3, P4);

        C22 = PointAdd(P5, P1);
        C22 = PointSub(C22, P3);
        C22 = PointSub(C22, P7);

        // Initializes the size of C's double array of data
        C->data = new int* [A.dim];
        for(int i = 0; i < A.dim; i++) {
            C->data[i] = new int[A.dim];
        }

        // Populates C using the four calculated subdivisions
        for (int i = 0; i < sub; i++) {
            for (int j = 0; j < sub; j++) {
                C->data[i][j] = C11->data[i][j];
                C->data[i][j + sub] = C12->data[i][j];
                C->data[i + sub][j] = C21->data[i][j];
                C->data[i + sub][j + sub] = C22->data[i][j];
            }
        }
    }

    // Returns final value of C, the calculated matrix multiplication
    return C;
}

/*
 * description: Multiplies two square matrices together using Strassen's method
 *              that requires only seven multiplications to find four
 *              subsections of the resulting matrix and then pieces them
 *              together. Threads the multiplications to speed up processing
 * return: SquareMatrix pointer
 * precondition: SquareMatrix A and B exist and are the same size
 * postcondition: returns the resulting matrix as a pointer
 */
SquareMatrix* ThreadedStrassen(const SquareMatrix& A, const SquareMatrix& B) {
    SquareMatrix *C = new SquareMatrix;

    // Creates seven threads to call the threaded version of brute force eight
    // times in order to speed up calculations
    pthread_t thread1, thread2, thread3, thread4, thread5, thread6, thread7;

    // Sets dimension of C
    C->dim = A.dim;

    // If it is a one by one matrix, then find the result using a single,
    // regular multiplication
    if(A.dim == 1) {
        C->data = new int* [A.dim];
        C->data[0] = new int[A.dim];
        C->data[0][0] = A.data[0][0] * B.data[0][0];
    } else {
        // Otherwise, prepare to find result using threaded Strassen's method
        int sub = A.dim / 2;

        // Square matrices created to subdivide matrices A, B, and C into four
        // parts to make calculations easier and less intensive.  Pieces the
        // parts of C together to find final result.
        SquareMatrix A11;
        SquareMatrix A12;
        SquareMatrix A21;
        SquareMatrix A22;

        SquareMatrix B11;
        SquareMatrix B12;
        SquareMatrix B21;
        SquareMatrix B22;

        SquareMatrix *C11 = new SquareMatrix;
        SquareMatrix *C12 = new SquareMatrix;
        SquareMatrix *C21 = new SquareMatrix;
        SquareMatrix *C22 = new SquareMatrix;

        // Square Matrices P1-P7 and S1-S10 are created to implement Strassen's
        // method. S1-S10 is used to calculated the P1-P7 matrices, while P1-P7
        // are used to only multiply seven times to find the subdivisions of C
        SquareMatrix *P1 = new SquareMatrix;
        SquareMatrix *P2 = new SquareMatrix;
        SquareMatrix *P3 = new SquareMatrix;
        SquareMatrix *P4 = new SquareMatrix;
        SquareMatrix *P5 = new SquareMatrix;
        SquareMatrix *P6 = new SquareMatrix;
        SquareMatrix *P7 = new SquareMatrix;

        SquareMatrix S1;
        SquareMatrix S2;
        SquareMatrix S3;
        SquareMatrix S4;
        SquareMatrix S5;
        SquareMatrix S6;
        SquareMatrix S7;
        SquareMatrix S8;
        SquareMatrix S9;
        SquareMatrix S10;

        // Sets all necessary dimensions of the matrices
        A11.dim = A12.dim = A21.dim = A22.dim = B11.dim = B12.dim = sub;
        B21.dim = B22.dim = C11->dim = C12->dim = C21->dim = C22->dim = sub;
        S1.dim = S2.dim = S3.dim = S4.dim = S5.dim = S6.dim = S7.dim = sub;
        S8.dim = S9.dim = S10.dim = sub;

        // Initializes the double array data in each matrix to the correct size
        // to ensure no out of bounds error
        A11.data = new int *[sub];
        A12.data = new int *[sub];
        A21.data = new int *[sub];
        A22.data = new int *[sub];

        B11.data = new int *[sub];
        B12.data = new int *[sub];
        B21.data = new int *[sub];
        B22.data = new int *[sub];


        for (int i = 0; i < sub; i++) {
            A11.data[i] = new int[sub];
            A12.data[i] = new int[sub];
            A21.data[i] = new int[sub];
            A22.data[i] = new int[sub];

            B11.data[i] = new int[sub];
            B12.data[i] = new int[sub];
            B21.data[i] = new int[sub];
            B22.data[i] = new int[sub];

            // After finishing initializing the size of data, then populates
            // the four subdivisions of A and B with the corresponding values
            for (int j = 0; j < sub; j++) {
                A11.data[i][j] = A.data[i][j];
                A12.data[i][j] = A.data[i][j + sub];
                A21.data[i][j] = A.data[i + sub][j];
                A22.data[i][j] = A.data[i + sub][j + sub];

                B11.data[i][j] = B.data[i][j];
                B12.data[i][j] = B.data[i][j + sub];
                B21.data[i][j] = B.data[i + sub][j];
                B22.data[i][j] = B.data[i + sub][j + sub];
            }
        }

        // Calculates each individual S1-S10 using addition or subtraction
        // functions for matrices. Standard add and sub are used here because
        // S1-S10 are SquareMatrices and not a pointer
        S1 = StanSub(B12, B22);
        S2 = StanAdd(A11, A12);
        S3 = StanAdd(A21, A22);
        S4 = StanSub(B21, B11);
        S5 = StanAdd(A11, A22);
        S6 = StanAdd(B11, B22);
        S7 = StanSub(A12, A22);
        S8 = StanAdd(B21, B22);
        S9 = StanSub(A11, A21);
        S10 = StanAdd(B11, B12);

        // Creates seven triple matrices to pass to the threading function of
        // brute force. Each triple matrix holds the necessary three matrices
        // to calculate and store a piece of C's four subdivisions
        TripleMatrix tmP1;
        TripleMatrix tmP2;
        TripleMatrix tmP3;
        TripleMatrix tmP4;
        TripleMatrix tmP5;
        TripleMatrix tmP6;
        TripleMatrix tmP7;

        // Prepares each triple matrix with the necessary matrices for
        // threading
        tmP1.A = &A11;
        tmP1.B = &S1;
        tmP1.C = P1;

        tmP2.A = &S2;
        tmP2.B = &B22;
        tmP2.C = P2;

        tmP3.A = &S3;
        tmP3.B = &B11;
        tmP3.C = P3;

        tmP4.A = &A22;
        tmP4.B = &S4;
        tmP4.C = P4;

        tmP5.A = &S5;
        tmP5.B = &S6;
        tmP5.C = P5;

        tmP6.A = &S7;
        tmP6.B = &S8;
        tmP6.C = P6;

        tmP7.A = &S9;
        tmP7.B = &S10;
        tmP7.C = P7;

        // Threads each call to the threaded brute force function to decrease
        // time necessary to calculate the multiplication by performing all
        // seven of the multiplications at once
        pthread_create(&thread1, nullptr, BruteForceSquareMatrixMultiplication, &tmP1);
        pthread_create(&thread2, nullptr, BruteForceSquareMatrixMultiplication, &tmP2);
        pthread_create(&thread3, nullptr, BruteForceSquareMatrixMultiplication, &tmP3);
        pthread_create(&thread4, nullptr, BruteForceSquareMatrixMultiplication, &tmP4);
        pthread_create(&thread5, nullptr, BruteForceSquareMatrixMultiplication, &tmP5);
        pthread_create(&thread6, nullptr, BruteForceSquareMatrixMultiplication, &tmP6);
        pthread_create(&thread7, nullptr, BruteForceSquareMatrixMultiplication, &tmP7);

        // Waits until all threads are finished and results have been stored
        pthread_join(thread1, nullptr);
        pthread_join(thread2, nullptr);
        pthread_join(thread3, nullptr);
        pthread_join(thread4, nullptr);
        pthread_join(thread5, nullptr);
        pthread_join(thread6, nullptr);
        pthread_join(thread7, nullptr);

        // Uses P1-P7 to find the subdivisions of C
        // Point sub and add are used because pointers are given as the
        // parameters and a pointer is returned.
        C11 = PointAdd(P5, P4);
        C11 = PointSub(C11, P2);
        C11 = PointAdd(C11, P6);

        C12 = PointAdd(P1, P2);
        C21 = PointAdd(P3, P4);

        C22 = PointAdd(P5, P1);
        C22 = PointSub(C22, P3);
        C22 = PointSub(C22, P7);

        // Initializes the size of C's double array of data
        C->data = new int* [A.dim];
        for(int i = 0; i < A.dim; i++) {
            C->data[i] = new int[A.dim];
        }

        // Populates C using the four calculated subdivisions
        for (int i = 0; i < sub; i++) {
            for (int j = 0; j < sub; j++) {
                C->data[i][j] = C11->data[i][j];
                C->data[i][j + sub] = C12->data[i][j];
                C->data[i + sub][j] = C21->data[i][j];
                C->data[i + sub][j + sub] = C22->data[i][j];
            }
        }
    }

    // Returns final value of C, the calculated matrix multiplication
    return C;
}

#endif //MATRIX_MULTIPLICATION_SQUAREMATRIX_H

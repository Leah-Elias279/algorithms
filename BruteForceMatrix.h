#ifndef BRUTEFORCEMATRIX_H_INCLUDED
#define BRUTEFORCEMATRIX_H_INCLUDED

class BruteForceMatrix {
public:
    int ** multiply(int **, int **, int);
};

#endif // BRUTEFORCEMATRIX_H_INCLUDED

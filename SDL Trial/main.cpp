#include <iostream>
#include <cmath>
#include <map>
#include "SDL_Plotter.h"
#include "squareMatrix.h"

using namespace std;

int newtonForm(int n, double xVal[3], double yVal[3], double toFind);

/*
 * description: Runs four different matrix multiplication algorithms,
 *              times them, then graphically displays the result
 * return: int to see if good execution
 * precondition: none
 * postcondition: Graphical representation is displayed
 *
*/
int main(int argc, char ** argv)
{

    SDL_Plotter g(1000,1000);
    bool stopped = false;
    int x, y;
    int xDisplace = 103;
    int yDisplace = 650;
    double avg = 0;
    double spot = 25;
    double Br[3];
    double DC[3];
    double S[3];
    double T[3];

    int xb, yb;

    bool brute, divide, thread, strass, cont = false;
    brute = divide = thread = strass = true;

    map<int, int> points;
    double xValBrute[10];
    double yValBrute[10];

    double xValDivC[10];
    double yValDivC[10];

    double xValStr[10];
    double yValStr[10];

    double xValThread[10];
    double yValThread[10];
    int result;

    while(!g.getQuit()) {
        while(!cont && !g.getQuit()) {
            if(g.kbhit()){
                switch(g.getKey()){
                    case 'B': brute = !brute;
                        break;
                    case 'T': thread = !thread;
                        break;
                    case 'S': strass = !strass;
                        break;
                    case 'D': divide = !divide;
                        break;
                    case 'C': cont = true;
                        break;
                    case 'X': g.setQuit(true);
                        break;
                }
            } else if(g.getMouseClick(xb,yb)){
                if(xb>50 && xb<100 && yb>50 && yb<100){
                    strass = !strass;
                    cout<<"white"<<endl;
                } else if(xb>700 && xb<750 && yb>50 && yb<100){
                    thread = !thread;
                    cout<<"red"<<endl;
                } else if(xb>700 && xb<750 && yb>700 && yb<750){
                    divide = !divide;
                    cout<<"blue"<<endl;
                } else if(xb>50 && xb<100 && yb>700 && yb<750){
                    brute = !brute;
                    cout<<"green"<<endl;
                }
            }
        }

        for(int i = 0; i < 1000; i++){
            for(int j = 0; j < 1000; j++){
                if(j>50 && j<100 && i>50 && i<100){
                    g.plotPixel(i,j,255,255,255);
                } else if(j>700 && j<750 && i>50 && i<100){
                    g.plotPixel(i,j,0,255,0);
                } else if(j>700 && j<750 && i>700 && i<750){
                    g.plotPixel(i,j,0,0,255);
                } else if(j>50 && j<100 && i>700 && i<750){
                    g.plotPixel(i,j,255,0,0);
                } else{
                    g.plotPixel(i,j,0,0,0);
                }
            }
        }

        int m = 32;
        int c = 0;

        if(brute) {
            //Get dynamic Brute Force
             while(m<=200) {
                for(int n=0;n<20;n++) {
                    SquareMatrix temp;

                    temp.dim = m;

                    temp.data = new int* [m];

                    for(int i=0;i<m;i++) {
                        temp.data[i] = new int[m];
                        for(int j=0;j<m;j++) {
                            temp.data[i][j] = i+j;
                        }
                    }


                    time_t start = clock();
                    BruteForce(temp, temp);
                    time_t finish = clock();

                    avg += difftime(finish, start);
                }

                Br[c] = (avg/20)*100;
                c++;
                avg = 0;

                m *= 2;
            }
        }

        if(divide) {
            m = 32;
            c = 0;
            while(m<200) {
                for(int n=0;n<20;n++) {
                    SquareMatrix temp;

                    temp.dim = m;

                    temp.data = new int* [m];

                    for(int i=0;i<m;i++) {
                        temp.data[i] = new int[m];
                        for(int j=0;j<m;j++) {
                            temp.data[i][j] = i+j;
                        }
                    }


                    time_t start = clock();
                    DivideAndConquer(temp, temp);
                    time_t finish = clock();

                    avg += difftime(finish, start);
                }


                DC[c] = (avg /20) * 100;
                cout << "DC: " << DC[c] << endl;
                c++;
                avg = 0;

                m *= 2;
            }
        }

        if(strass) {
            m = 32;
            c = 0;
            while(m < 200){
                for(int n = 0; n < 20; n++){
                    SquareMatrix temp;

                    temp.dim = m;
                    temp.data = new int* [m];

                    for(int i = 0; i < m; i++){
                        temp.data[i] = new int[m];
                        for(int j = 0; j < m; j++){
                            temp.data[i][j] = i+j;
                        }
                    }
                    time_t start = clock();
                    Strassen(temp, temp);
                    time_t finish = clock();

                    avg += difftime(finish, start);
                }
                cout << (avg / 20) << endl;
                S[c] = (avg/20) * 100;
                c++;
                avg = 0;

                m *= 2;
            }
        }

        if(thread) {
            m = 32;
            c = 0;
            while(m<200) {
                for(int n=0;n<20;n++) {
                    SquareMatrix temp;

                    temp.dim = m;

                    temp.data = new int* [m];

                    for(int i=0;i<m;i++) {
                        temp.data[i] = new int[m];
                        //cout << "testing" << endl;
                        for(int j=0;j<m;j++) {
                            temp.data[i][j] = i+j;
                        }
                    }


                    time_t start = clock();
                    ThreadedDivideAndConquer(temp, temp);
                    time_t finish = clock();

                    avg += difftime(finish, start);
                }

                //cout << (avg / 100) << endl;

                T[c] = (avg/20) *100;
                cout << "T: " << T[c] << endl;
                c++;
                avg = 0;

                m *= 2;
            }
        }
        //----------------------------------
        for(spot = 25; spot < 120; spot++) {
            if(brute) {

                //Brute force
                xValBrute[0] = 0;
                xValBrute[1] = 32;
                xValBrute[2] = 64;
                xValBrute[3] = 128;

                yValBrute[0] = 0;
                yValBrute[1] = Br[0];
                yValBrute[2] = Br[1];
                yValBrute[3] = Br[2];

                if(spot != 0){
                    result = newtonForm(4, xValBrute, yValBrute, spot);
                }else{
                    result = 0;
                }
                int temp = spot*6 - 125;

                //points[spot + xDisplace - 25] = -1 * (result - 1000) - 340;

                if((-1*(result - 1000) - 344) >= 150){
                    g.plotPixel(temp + xDisplace - 25, -1 * (result - 1000) - 340, 0, 255, 0);
                    g.plotPixel(temp + xDisplace - 24, -1 * (result - 1000) - 341, 0, 255, 0);
                    g.plotPixel(temp + xDisplace - 23, -1 * (result - 1000) - 342, 0, 255, 0);
                    g.plotPixel(temp + xDisplace - 22, -1 * (result - 1000) - 343, 0, 255, 0);
                    g.plotPixel(temp + xDisplace - 21, -1 * (result - 1000) - 344, 0, 255, 0);
                }
            }

            if(divide) {
                xValDivC[0] = 0;
                xValDivC[1] = 32;
                xValDivC[2] = 64;
                xValDivC[3] = 128;

                yValDivC[0] = 0;
                yValDivC[1] = DC[0];
                yValDivC[2] = DC[1];
                yValDivC[3] = DC[2];

                if(spot != 0){
                    result = newtonForm(4, xValDivC, yValDivC, spot);
                }else{
                    result = 0;
                }
                int temp = spot*6 - 125;

                if((-1*(result - 1000) - 344) >= 150){
                    g.plotPixel(temp + xDisplace - 25, -1 * (result - 1000) - 340, 0, 0, 255);
                    g.plotPixel(temp + xDisplace - 24, -1 * (result - 1000) - 341, 0, 0, 255);
                    g.plotPixel(temp + xDisplace - 23, -1 * (result - 1000) - 342, 0, 0, 255);
                    g.plotPixel(temp + xDisplace - 22, -1 * (result - 1000) - 343, 0, 0, 255);
                    g.plotPixel(temp + xDisplace - 21, -1 * (result - 1000) - 344, 0, 0, 255);
                }
            }

            if(strass) {
                xValStr[0] = 0;
                xValStr[1] = 32;
                xValStr[2] = 64;
                xValStr[3] = 128;

                yValStr[0] = 0;
                yValStr[1] = S[0];
                yValStr[2] = S[1];
                yValStr[3] = S[2];

                if(spot != 0){
                    result = newtonForm(4, xValStr, yValStr, spot);
                }else{
                    result = 0;
                }
                int temp = spot*6 - 125;

                if((-1*(result - 1000) - 344) >= 150){
                    g.plotPixel(temp + xDisplace - 25, -1 * (result - 1000) - 340, 255, 255, 255);
                    g.plotPixel(temp + xDisplace - 24, -1 * (result - 1000) - 341, 255, 255, 255);
                    g.plotPixel(temp + xDisplace - 23, -1 * (result - 1000) - 342, 255, 255, 255);
                    g.plotPixel(temp + xDisplace - 22, -1 * (result - 1000) - 343, 255, 255, 255);
                    g.plotPixel(temp + xDisplace - 21, -1 * (result - 1000) - 344, 255, 255, 255);
                }
            }

            if(thread){
                xValThread[0] = 0;
                xValThread[1] = 32;
                xValThread[2] = 64;
                xValThread[3] = 128;

                yValThread[0] = 0;
                yValThread[1] = T[0];
                yValThread[2] = T[1];
                yValThread[3] = T[2];

                if(spot != 0){
                    result = newtonForm(4, xValThread, yValThread, spot);
                }else{
                    result = 0;
                }
                int temp = spot *6 - 125;

                if((-1*(result - 1000) - 274) >= 150){
                    g.plotPixel(temp + xDisplace - 25, -1 * (result - 1000) - 270, 255, 0, 0);
                    g.plotPixel(temp + xDisplace - 24, -1 * (result - 1000) - 271, 255, 0, 0);
                    g.plotPixel(temp + xDisplace - 23, -1 * (result - 1000) - 272, 255, 0, 0);
                    g.plotPixel(temp + xDisplace - 22, -1 * (result - 1000) - 273, 255, 0, 0);
                    g.plotPixel(temp + xDisplace - 21, -1 * (result - 1000) - 274, 255, 0, 0);
                }
            }
        }
    //-----------------------------------
    //Get Dynamic Divide and Conquer
    //-----------------------------------


    //-----------------------------
    //Get Dynamic Strassen
    //-----------------------------


    //-----------------------------
    //Get Dynamic Thread
    //-----------------------------

    //-----------------------------
        if(!stopped){
            //plot y axis
            for(int i = 0; i < 500; i++){
                g.plotPixel(100, i + 150, 255, 255, 255);
                g.plotPixel(101, i + 150, 255, 255, 255);
                g.plotPixel(102, i + 150, 255, 255, 255);
            }
            //plot x axis
            for(int i = 0; i < 600; i++){
                g.plotPixel(100 + i, 650, 255, 255, 255);
                g.plotPixel(100 + i, 651, 255, 255, 255);
                g.plotPixel(100 + i, 652, 255, 255, 255);
            }
        }

        if(g.kbhit()){
            g.getKey();
        }

        if(g.getMouseClick(x,y)){
            stopped = !stopped;
        }


        //Divide and Conquer


        //Strassen

        //Thread


        /*
        //plot diagonal line
        if(spot != 500){
            g.plotPixel(xDisplace + spot, yDisplace - spot, 250, 0, 0);
            spot++;
        }
        */


        g.update();
        cont = false;
        g.clear();
    }
    return 0;
}

/*
 * description: converts the points provided by the runtime into a graph
 * return: int
 * precondition: We have run the algorithm to find average times
 * postcondition: returns the y-value
 *
*/
int newtonForm(int n, double xVal[3], double yVal[3], double toFind){
    int i, j;
    double sum = 0, mult = 0;

    for(j=0;j<n-1;j++){
        for(i=n-1;i>j;i--)
            yVal[i] = (yVal[i]-yVal[i-1])/(xVal[i]-xVal[i-j-1]);
    }
    for(i=n-1;i>=0;i--){
        mult=1;
        for(j=0;j<i;j++)
            mult*=(toFind-xVal[j]);

        mult*=yVal[j];
        sum+=mult;
    }
    return static_cast<int>(sum);
}
